import React, { Component } from 'react'
import { Text, View, StyleSheet, Button } from 'react-native';
import 'react-native-gesture-handler';

export class Details extends Component {
    state = {}

    render() {
       // console.log("this is props =====> ", this.props.route.params.text);
        return (
            <View style={styles.container}>
                <Text style={styles.m10}>Details Page</Text>
                <View style={styles.m10}>
                    <Button
                        title="change Title"
                        onPress={() => this.props.navigation.setOptions({title : "updated"})} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        marginHorizontal: 16,
    },
    m10: {
        marginVertical: 10,
    },

});

export default Details

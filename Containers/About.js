import React, { Component } from 'react'
import { Text, View, StyleSheet, Button , FlatList ,TouchableOpacity} from 'react-native';
import 'react-native-gesture-handler';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
export class About extends Component {
    state = {
        user: {
            username: '',
            password: ''
        },
        products : [],
        page : 1
    }
    componentDidMount() {
        const usr = AsyncStorage.getItem('username', (error, result) => {
            //console.log('this is username ==>', result);
            this.setState(prevState => {
                return {
                    user: {
                        ...prevState.user,
                        username: result
                    }
                }
            })
        });
        const pass = AsyncStorage.getItem('password', (error, result) => {
            //console.log('this is password ==>', result);
            this.setState(prevState => {
                return {
                    user: {
                        ...prevState.user,
                        password: result
                    }
                }
            })
        });
        //alert("about page");
      this.getPosts();
    }
    getPosts(){
        try {
            axios
                .get(`http://roocket.org/api/products?page=${this.state.page}`)
                .then( (response) => {
                    // handle success
                    //alert(JSON.stringify(response.data.data));
                    this.setState({
                        products : response.data.data.data
                    })
                     console.log("products ====> ",this.state.products);
                })
                .catch(function (error) {
                    // handle error
                    alert(error.message);
                });

        } catch (error) {
            console.log(error)
        }
    }
    renderItem(item){
        console.log("item =>",item.item)
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.m10}>username : {this.state.user.username}</Text>
                <Text style={styles.m10}>password : {this.state.user.password}</Text>
                <FlatList 
                data={this.state.products}
                //renderItem={this.renderItem}

                renderItem={({ item: p }) => (
                    <TouchableOpacity>
                      <Text>
                        {p.title}
                      </Text>
                    </TouchableOpacity>
                  )}


                />
                <View style={styles.m10}>
                    <Button
                        title="home page"
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
                </View>
                <View style={styles.m10}>
                    <Button
                        title="back"
                        onPress={() => this.props.navigation.goBack()}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        marginHorizontal: 16,
    },
    m10: {
        marginVertical: 10,
    },

});

export default About

import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import {
    Container,
    Header,
    Button,
    Text,
    Left,
    Right,
    Form,
    Content,
    Item,
    Input,
    Icon,
} from 'native-base';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
export class Home extends Component {
    state = {
        user: {
            username: '',
            password: ''
        }
    }
    componentDidMount() {
        this.setState({
            user: {
                username: "javad",
                password: "8659138"
            }
        })
    }
    changeUsername(text) {
        this.setState(prevState => {
            return {
                user: {
                    ...prevState.user,
                    username: text
                }
            }
        }, () => { console.log('state =>', this.state.user); })
    }
    changePassword(text) {
        this.setState(prevState => {
            return {

                user: {
                    ...prevState.user,
                    password: text
                }
            }
        })
        console.log('state =>', this.state.user);
    }
    submit() {
        AsyncStorage.setItem('username', this.state.user.username);
        AsyncStorage.setItem('password', this.state.user.password);
        //console.log(AsyncStorage.getItem('username'))
        const value = AsyncStorage.getItem('username', (error, result) => { });

        // try {
        //     axios
        //         .post('http://192.168.1.101:8000/api/auth/login', {
        //             username: "09111111111",
        //             password: "123456",
        //             isPanel: true
        //         })
        //         .then(function (response) {
        //             // handle success
        //             alert(JSON.stringify(response.data));
        //         })
        //         .catch(function (error) {
        //             // handle error
        //             alert(error.message);
        //         });

        // } catch (error) {
        //     console.log(error)
        // }

        this.props.navigation.navigate('About');
    }

    render() {
        return (
            <Container>
                <Text style={{ fontFamily: "IRANSans" }, styles.m10}>به صفحه ورود خوش آمدید.</Text>
                <Content>
                    <Form>
                        <Item>
                            <Input
                                placeholder="Username"
                                defaultValue={this.state.user.username}
                                onChangeText={this.changeUsername.bind(this)}
                            />
                        </Item>
                        <Item last>
                            <Input
                                placeholder="Password"
                                defaultValue={this.state.user.password}
                                onChangeText={this.changePassword.bind(this)}
                            />
                        </Item>
                        <Button primary onPress={this.submit.bind(this)}>
                            <Text style={{ fontFamily: "IRANSans" }}>ورود</Text>
                        </Button>
                    </Form>
                </Content>

            </Container>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        marginHorizontal: 16,

    },
    m10: {
        marginVertical: 10,
    },
    gry: {
        backgroundColor: '#eeeeee'
    }
});

export default Home

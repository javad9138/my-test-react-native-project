import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SectionList, Button } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Containers/Home';
import About from './Containers/About';
import Details from './Containers/Details';

const Stack = createStackNavigator();


export class App extends Component {
    state = {}

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator screenOptions={{
                    headerTintColor: "#fff",
                    headerStyle: {
                        backgroundColor: "blue"
                    },
                }}>
                    <Stack.Screen
                        name="Home"
                        component={Home}
                        options={{
                            title: "This is Home Page",
                            headerTitleAlign: "center",
                            headerTitleStyle: {
                                fontWeight: "bold",
                                fontSize: 14
                            }
                        }}
                    />
                    <Stack.Screen
                        name="About"
                        component={About}
                        options={{
                            headerRight: () => (
                                <Button
                                    title="btn title"
                                    color="orange"
                                    onPress={() => alert("hiii btn")}
                                />
                            ),
                            headerRightContainerStyle: {
                                marginHorizontal: 10
                            }
                        }}
                    />
                    <Stack.Screen
                        name="Details"
                        component={Details}
                        options={{
                            headerTitleAlign: "center",
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        marginHorizontal: 16,
        // alignItems: "center"
        //     justifyContent : 'center',
        //     flexDirection : 'row'
    },
});

export default App
